import re
import sys

maxLoopIter = {}

def get_flowfacts(filename):
    global maxLoopIter
    
    name = re.match(".+\.elf$", filename)
    if name: # bench.elf -> bench.ff
        #print("match")
        #print(name[0])
        fffilename = filename[:-3] + "ff"
    else: # bench -> bench.ff
        #print("not match")
        fffilename = filename + ".ff"
    
    with open(fffilename) as f:
        line = f.readline()
        while line:
            #print(line)
            #result1 = re.match("\t*loop[^;]*; // (\w+)", line)
            #result2 = re.match("\t*loop[^;]* (\d+); // ", line)
            result1 = re.match(" *loop[^;]*; // (\w+)", line)
            result2 = re.match(" *loop[^;]* (\d+); // ", line)
            #if result1 and result1.lastindex > 0 and result2 and result2.lastindex > 0:
            if result1 and result1.lastindex > 0:
                #print(result1.group(1))
                loopaddr = int(result1.group(1),16)
                #print(loopaddr)
                if result2 and result2.lastindex > 0:
                    #print(result.group(1))
                    maxiter = int(result2.group(1))
                    #print(maxiter)
                    maxLoopIter[loopaddr] = maxiter
                else:
                    print("Flow-facts not correctly set in", fffilename)
                    sys.exit(-1)
            line = f.readline()
    
# test
#get_flowfacts("bsort_O2.elf")
#get_flowfacts("matmult_O1_alba.elf")
#get_flowfacts("ludcmp_O3_arm")
#get_flowfacts("matmult_03.elf")

#print(maxLoopIter)
