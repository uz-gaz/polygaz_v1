
SOURCES := $(wildcard *.c)
OBJECTS := $(SOURCES:.c=.o)
GIT_VERSION := "$(shell git describe --dirty --always --tags)"
#APRONPATH=/home/jsegarra/apron

%.o: %.c %.h
	gcc -Wall -c $< -O3 -fPIC -std=c99 -DPOLYGAZ_VERSION=\"$(GIT_VERSION)\" # -I$(APRONPATH)/include

libpolygaz.so: $(OBJECTS)
	gcc -Wall -shared -fPIC -o $@ $(OBJECTS) -lapron -lpolkaMPQ -loctMPQ -lap_ppl -lgmp -lmpfr

clean:
	-rm -f libpolygaz.so $(OBJECTS)
