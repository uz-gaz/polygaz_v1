# polygaz_v1

Code for obtaining safe data access patterns and reuse information for WCET analysis using Abstract Interpretation.

If you use this code, please provide the following reference:

J. Segarra, J. Cortadella, R. Gran Tejero and V. Viñals-Yúfera, "Automatic Safe Data Reuse Detection for the WCET Analysis of Systems With Data Caches," in IEEE Access, vol. 8, pp. 192379-192392, 2020, doi: 10.1109/ACCESS.2020.3032145.


```
@ARTICLE{9229458,
 author={J. {Segarra} and J. {Cortadella} and R. {Gran Tejero} and V. {Viñals-Yúfera}},
 journal={IEEE Access},
 title={Automatic Safe Data Reuse Detection for the WCET Analysis of Systems With Data Caches},
 year={2020},
 volume={8},
 number={},
 pages={192379-192392},
 doi={10.1109/ACCESS.2020.3032145}
}
```

## Licencia

This code is licensed under *GNU General Public License* (version 3) **WITH THE FOLLOWING RESTRICTION**:

*The use, investigation or development, in a direct or indirect way, of any of the scientific contributions of the authors contained in this work by any army or armed group in the world, for military purposes and for any other use which is against human rights or the environment, is strictly prohibited unless written consent is obtained from all the authors of this work, or all the people in the world.*


## Instalación (incluyendo [angr](http://angr.io/) y [apron](https://apron.cri.ensmp.fr/library/))


### en Debian

1.  `git clone https://gitlab.com/uz-gaz/polygaz_v1.git polygaz` e ir al directorio base.
2.  `virtualenv --python=$(which python3) venv` crea un entorno virtual para trabajar en el subdirectorio *venv*. El comando virtualenv equivale a `python3 -m venv venv`.
En `venv/bin/` se creará el ejecutable de python a usar.
3.  `./venv/bin/pip3 install angr` busca, descarga, e instala para el usuario (no para el sistema) todo lo necesario.
4.  `apt install libapron libapron-dev`


### en CentOS 7 (funciona con python3.6, NO funciona con 3.4)

1.  `yum install epel-release python36 python36-pip python36-devel gmp-devel mpfr-devel parallel`
2.  `git clone https://gitlab.com/uz-gaz/polygaz_v1.git polygaz` e ir al directorio base.
3.  `virtualenv --python=$(which python3) venv` crea un entorno. virtual para trabajar en el subdirectorio *venv*. El comando virtualenv equivale a `python3 -m venv venv`.
En `venv/bin/` se creará el ejecutable de python a usar.
4.  `./venv/bin/pip3 install angr` busca, descarga, e instala para el usuario (no para el sistema) todo lo necesario.
5.  Bajar/compilar/instalar [apron](https://antoinemine.github.io/Apron/doc/) ([fuentes GitHub](https://github.com/antoinemine/apron), [fuentes Debian](http://deb.debian.org/debian/pool/main/a/apron/apron_0.9.10.orig.tar.gz)).
6.  No instala bien, copiar manualmente en ~/apron:
    1.  `find . -name lib* -exec cp \{\} ~/apron/lib \;`
    2.  `find . -name *.h -exec cp \{\} ~/apron/include/ \;`
7.  Editar Makefile de polygaz.
Añadir -I$HOME/apron/include en el comando de compilación y -L$HOME/apron/lib en el de enlazado.
Si no tiene Parma Polyhedra, quitar -lap_ppl y comentar líneas en apron_interface.c.
8.  Antes de ejecutar, ajustar LD_LIBRARY_PATH: `export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/apron/lib`


## Ejecución

-   `./venv/bin/python3 mkff.py <binario-a-analizar>` genera una plantilla de *flowfacts*. Hay que editar manualmente los '?' con el número máximo de iteraciones de cada bucle.
-   Con prints de depuración: `make && ./venv/bin/python3 -u analysis1.py <binario-a-analizar>`
-   Sin depuración (comentar #defines DEBUG en .h): `make && ./venv/bin/python3 -O analysis1.py <binario-a-analizar>`
