#!angr/bin/python3
# coding=utf-8

import sys
import resource # to set stack limit to unlimited (ulimit -s unlimited)
#from ctypes import *
from ctypes import CDLL, c_int, c_char_p, c_double, c_void_p, byref
#CDLL("/usr/lib/libpolkaMPQ.so.0")
#CDLL("/usr/lib/libapron.so.0")
import time
import networkx
import angr # grafo
import claripy # estado de memoria
import pyvex # irVEX

import indvars
import flowfacts

#import logging

DEBUG_ASM = True
DEBUG_IRVEX = True
DEBUG_FIXPOINT = True
DEBUG_FIXPOINT_INDVARS = True
DEBUG_MEM = True
DEBUG_COMPARE = True
DEBUG_COST = True
#DEBUG_ASM = False
#DEBUG_IRVEX = False
#DEBUG_FIXPOINT = False
#DEBUG_FIXPOINT_INDVARS = False
#DEBUG_MEM = False
#DEBUG_COMPARE = False
#DEBUG_COST = False



################################ INIT ################################
def init_apron(numnodes, domain):
    """Intializes apron interface"""
    # load apron interface
    #apron = CDLL("./apron_interface.so")

    # init apron_interface args
    apron.init.argtypes = [c_int, c_int]
    apron.addRegToExpr.argtypes = [c_int, c_char_p]
    apron.assignExprToReg.argtypes = [c_int]
    apron.assignExprToTemp.argtypes = [c_int]
    apron.assignExprToMem.argtypes = [c_int]
    apron.varExists.argtypes = [c_char_p]
    apron.varExists.restype = c_int
    apron.newNodeState.argtypes = [c_int, c_int]
    apron.setOutState.argtypes = [c_int]
    apron.addMemRef.argtypes = [c_int, c_int, c_int, c_char_p, c_int, c_int, c_int, c_int]
    apron.addMemConstRef.argtypes = [c_int, c_int, c_int, c_int, c_int, c_int, c_int]
    apron.addMemRefOffset.argtypes = [c_int, c_int, c_int, c_char_p, c_int, c_int]
    #apron.getInductionPC.argtypes = [c_int]
    #apron.getInductionPC.restype = c_int
    #apron.getInductionReg.argtypes = [c_int]
    #apron.getInductionReg.restype = c_int
    #apron.getInductionLoop.argtypes = [c_int]
    #apron.getInductionLoop.restype = c_int
    #apron.isIndVar.restype = c_int
    #apron.prepareInduction.argtypes = [c_int, c_int]
    #apron.prepareOuterLoopInduction.argtypes = [c_int]
    #apron.getFirstGR.argtypes = [c_int]
    #apron.getFirstGR.restype = c_int
    apron.getLastGR.argtypes = [c_int]
    apron.getLastGR.restype = c_int
    #apron.setFirstGR.argtypes = [c_int, c_int]
    apron.setLastGR.argtypes = [c_int, c_int]
    apron.getIndexMemRefFirst.argtypes = [c_int]
    apron.getIndexMemRefFirst.restype = c_int
    apron.getIndexMemRefLast.argtypes = [c_int]
    apron.getIndexMemRefLast.restype = c_int
    apron.orderedMemRefs.argtypes = [c_int, c_int]
    apron.orderedMemRefs.restype = c_int
    apron.memRefIsConditional.argtypes = [c_int]
    apron.memRefIsConditional.restype = c_int
    apron.nodeExists.argtypes = [c_int, c_int]
    apron.nodeExists.restype = c_int
    #apron.inductionSetResult.argtypes = [c_int, c_int]
    apron.addNestInfo.argtypes = [c_int, c_int, c_int, c_int, c_int]
    #apron.addNestInfo.argtypes = [c_int, c_int, c_int, c_int, c_int, c_int, c_int]
    apron.getVariableSupInterval.argtypes = [c_char_p]
    apron.getVariableSupInterval.restype = c_double
    apron.getVariableInfInterval.argtypes = [c_char_p]
    apron.getVariableInfInterval.restype = c_double
    apron.sameBoundsDifferenceInterval.argtypes = [c_char_p, c_char_p, c_void_p]
    apron.sameBoundsDifferenceInterval.restype = c_int
    apron.sameBoundsVariable.argtypes = [c_char_p, c_void_p]
    apron.sameBoundsVariable.restype = c_int

    if domain == "oct":
        apron.init(2*numnodes, 1) # oct
    elif domain == "pk":
        apron.init(2*numnodes, 2) # pk
    elif domain == "ap_ppl":
        apron.init(2*numnodes, 3) # ap_ppl
    elif domain == "pkeq":
        apron.init(2*numnodes, 4) # pkeq
    else:
        print("Unknown domain", domain, "(valid: oct|pk|pkeq|ap_ppl)")
        sys.exit(-1)
    # numnodes for the general analysis + numregs*numnodes for indvar analysis
    #apron.setNumLoops(numloops)
#    apron.init(numnodes, numloops)

# # init logger (after angr CFG/LoopFinder analysis)
# logger = logging.getLogger()
# #logger.setLevel(logging.CRITICAL)
# #logger.setLevel(logging.ERROR)
# #logger.setLevel(logging.WARNING)
# #logger.setLevel(logging.INFO)
# logger.setLevel(logging.DEBUG)
# ch = logging.StreamHandler()
# formatter = logging.Formatter('%(levelname)s: %(message)s')
# ch.setFormatter(formatter)
# logger.addHandler(ch)

def init_regs():
    """Intialize known registers"""
    global registers # registers to explore for induction variables
    # ARM gcc:
    # (r1): r12
    # (r2): r16
    # (r3): r20
    # (fp): r52 # Frame Pointer
    # (sp): r60=0x80000000 # Stack Pointer
    for reg in range(0, 16):
        if reg != 15: # exclude PC (15)
            registers.append((reg+2)*4) # ARM GCC

#    apron.initExpr()
#    apron.addConstToExpr(0x80000000) # address printing is modulo 32
#    apron.assignExprToReg(60)
    apron.initStack(60, 0x80000000)
    # (pc): r68 # Program Counter
    # condition?: r72,r76,r80,r84,
    # condition?: r392 ==> appears with ARM THUMB: untested, compile with -marm


## Processes a CFG node. Assumes global aistate for the initial state
def generateOutState(CFGNode):
    """Process a CFG node. Assume global aistate for the initial state."""
    global aistate, ncomparisons
    global anyStoreToUnknownAddr, state
    global reginreg, regintemp
    basicBlock = CFGNode.block
    if __debug__ and DEBUG_ASM:
        print("-----instrucciones-----")
        basicBlock.pp()
        print("-----------------------")
    irsb = basicBlock.vex
    ncomparisons = 0
    for stmt in irsb.statements:
        if __debug__ and DEBUG_IRVEX:
            stmt.pp()
        if isinstance(stmt, pyvex.IRStmt.NoOp):
            # print(" No operation") # nada
            pass
        elif isinstance(stmt, pyvex.IRStmt.IMark):
            # print(" New instruction", stmt) # nueva instrucción hardware; nada
            currentPC = stmt.addr
        elif isinstance(stmt, pyvex.IRStmt.AbiHint):
            stmt.pp()
            print(" ABI hint") # nada?
            sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.Put):
            # print(" Escritura en registro:",)
            studyExpr(stmt.data, currentPC, CFGNode)
            #studyExpr(stmt.data, basicBlock.addr, currentPC)
            apron.assignExprToReg(stmt.offset)
            # nou
            reg = apron.getRegInExpr()

            if reg != -1:
                if reg != stmt.offset:
                    reginreg[stmt.offset] = reg
                    #print("reg", stmt.offset, "actualizado con reg", reg)
            else:
                temp = apron.getTempInExpr()
                if temp != -1:
                    if temp in regintemp:
                        if stmt.offset != regintemp[temp]:
                            reginreg[stmt.offset] = regintemp[temp]
                            #print("reg", stmt.offset, "actualizado con reg", reginreg[stmt.offset])
                    else:
                        if stmt.offset in reginreg:
                            del reginreg[stmt.offset]
                else:
                    if stmt.offset in reginreg:
                        del reginreg[stmt.offset]

        elif isinstance(stmt, pyvex.IRStmt.PutI):
            stmt.pp()
            print(" Escritura en bits de registro no implementado")
            sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.WrTmp):
            # print(" Escritura en registro virtual temporal")
            studyExpr(stmt.data, currentPC, CFGNode)
            #studyExpr(stmt.data, basicBlock.addr, currentPC)
            apron.assignExprToTemp(stmt.tmp)
            # nou
            reg = apron.getRegInExpr()
            if reg != -1:
                regintemp[stmt.tmp] = reg
                #print("temp", stmt.tmp, "actualizado con reg", reg)
            else:
                temp = apron.getTempInExpr()
                if temp != -1:
                    if temp in regintemp:
                        regintemp[stmt.tmp] = regintemp[temp]
                        #print("temp", stmt.tmp, "actualizado con reg", regintemp[temp])
                    else:
                        if stmt.tmp in regintemp:
                            del regintemp[stmt.tmp]
                else:
                    if stmt.tmp in regintemp:
                        del regintemp[stmt.tmp]
            #regName = "t"+str(stmt.tmp)
            #apron.assignExpr(regName.encode('utf-8'))
        elif isinstance(stmt, pyvex.IRStmt.Store):
            if __debug__ and DEBUG_MEM:
                print(" Escritura en memoria, PC=", hex(currentPC),
                    ", addr=", stmt.addr, "data=", stmt.data)
            # TODO comprobar tamaño de dato
            addr = processStore(stmt.addr, 'Ity_I32', stmt.data)
            addMemAccess(CFGNode, currentPC, stmt.addr, addr, 4, 0, 0)
            #addMemAccess(basicBlock.addr, currentPC, stmt.addr, 4)
        elif isinstance(stmt, pyvex.IRStmt.CAS):
            stmt.pp()
            print(" Atomic compare-and-swap no impl")
            sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.LLSC):
            stmt.pp()
            print(" Load-linked (STOREDATA=NULL) or Store-Conditional no impl")
            sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.MBE):
            stmt.pp()
            print(" Memory Bus Event? no impl")
            sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.Dirty):
            stmt.pp()
            print(" Dirty? no impl")
            sys.exit(-1)
        elif isinstance(stmt, pyvex.IRStmt.Exit):
            #print(" Conditional Exit from IRSB; no impl",)
            #stmt.guard.pp() # registro donde está el resultado de la condición
            #print(" Target:", stmt.dst.pp()) # destino de salto
            pass
        elif isinstance(stmt, pyvex.IRStmt.LoadG):
            #stmt.pp()
            if __debug__ and DEBUG_MEM:
                #print(" Lectura CONDICIONADA en memoria, PC=", hex(currentPC), ", addr=", stmt.addr, ", cond=", stmt.guard, "dest:", stmt.dst, "val:", constval)
                print(" Lectura CONDICIONADA en memoria, PC=", hex(currentPC),
                    ", addr=", stmt.addr, ", cond=", stmt.guard, "dest:", stmt.dst)
            # TODO comprobar tamaño de dato
            addr = processLoad(stmt.addr, 'Ity_I32')
            addMemAccess(CFGNode, currentPC, stmt.addr, addr, 4, 1, 1)
            #addMemAccess(basicBlock.addr, currentPC, stmt.addr, 4)
            if ncomparisons > 0:
                ncomparisons = ncomparisons - 1
                if __debug__ and DEBUG_COMPARE:
                    print("Warning: comparación perdida")
#            apron.initExpr()
#            if anyStoreToUnknownAddr:
#                apron.addTopToExpr()
#            else:
#                memval = state.mem[stmt.addr].int32_t.resolved
#                print("Warning: assuming int32_t (", memval, ") in load from", hex(stmt.addr))
#                apron.addConstToExpr(memval)
#            apron.assignExprToTemp(stmt.dst)
#            # # regName = "r"+str(stmt.offset)
#            #regName = "t"+str(stmt.dst)
#            #apron.assignExpr(regName.encode('utf-8'))
        elif isinstance(stmt, pyvex.IRStmt.StoreG):
            #stmt.pp()
            if __debug__ and (DEBUG_MEM or DEBUG_COMPARE):
                print(" Escritura CONDICIONADA en memoria, PC=", hex(currentPC),
                    ", addr=", stmt.addr, ", cond=", stmt.guard)
            # TODO comprobar tamaño de dato
            addr = processStore(stmt.addr, 'Ity_I32', stmt.data)
            addMemAccess(CFGNode, currentPC, stmt.addr, addr, 4, 0, 1)
            #addMemAccess(basicBlock.addr, currentPC, stmt.addr, 4)
            if ncomparisons > 0:
                ncomparisons = ncomparisons - 1
                if __debug__ and DEBUG_COMPARE:
                    print("Warning: comparación perdida")
        else:
            stmt.pp()
            print("generateOutState: Unknown statement:")
            sys.exit(-1)
    return ncomparisons


#def processStore(target, ty, reg):
#    global anyStoreToUnknownAddr
#    global mem
#
#    if isinstance(target, pyvex.IRExpr.Const):
#        addr = target.con.value
#    else:
#        if isinstance(target, pyvex.IRExpr.RdTmp):
#            regName = "t"+str(target.tmp)
#        elif isinstance(target, pyvex.IRExpr.Get):
#            regName = "r"+str(target.offset)
#        else:
#            print("processLoad: memRef type not implemented")
#            target.pp()
#            sys.exit(-1)
#        regNameEnc = regName.encode('utf-8')
#        supBound = apron.getVariableSupInterval(regNameEnc)
#        infBound = apron.getVariableInfInterval(regNameEnc)
#        if infBound == supBound:
#            addr = infBound
#        else:
#            addr = None
#
#    if addr == None:
#        anyStoreToUnknownAddr = True
#        mem = {}
#    else:
#        if isinstance(reg, pyvex.IRExpr.Const):
#            mem[addr] = reg.con.value
#        else:
#            if isinstance(reg, pyvex.IRExpr.RdTmp):
#                regName = "t"+str(target.tmp)
#            elif isinstance(reg, pyvex.IRExpr.Get):
#                regName = "r"+str(target.offset)
#            else:
#                print("processStore: memRef type not implemented")
#                reg.pp()
#                sys.exit(-1)
#            regNameEnc = regName.encode('utf-8')
#            supBound = apron.getVariableSupInterval(regNameEnc)
#            infBound = apron.getVariableInfInterval(regNameEnc)
#            if infBound == supBound:
#                mem[addr] = infBound
#            else:
#                mem[addr] = None # Top
#                # TODO specify the size of the stored data


#def processLoad(target, ty):
#    global anyStoreToUnknownAddr
#    global mem
#
#    apron.initExpr()
#    if isinstance(target, pyvex.IRExpr.Const):
#        addr = target.con.value
#    else:
#        if isinstance(target, pyvex.IRExpr.RdTmp):
#            regName = "t"+str(target.tmp)
#        elif isinstance(target, pyvex.IRExpr.Get):
#            regName = "r"+str(target.offset)
#        else:
#            print("processLoad: memRef type not implemented")
#            target.pp()
#            sys.exit(-1)
#        regNameEnc = regName.encode('utf-8')
#        supBound = apron.getVariableSupInterval(regNameEnc)
#        infBound = apron.getVariableInfInterval(regNameEnc)
#        if infBound == supBound:
#            addr = infBound
#        else:
#            addr = None
#    if addr is not None:
#        if addr in mem:
#            constval = mem[addr]
#        elif anyStoreToUnknownAddr:
#            constval = None
#        else:
#            if ty == 'Ity_I32':
#                memval = state.mem[addr].int32_t.resolved
#            elif ty == 'Ity_I16':
#                memval = state.mem[addr].int16_t.resolved
#            else:
#                print("unknown size in load to", addr)
#                sys.exit()
#            constval = state.solver.eval(memval)
#    else:
#        constval = None
#
#    if constval is None:
#        apron.addTopToExpr()
#    else:
#        apron.addConstToExpr(constval)
#
#    return constval

def processStore(target, ty, reg):
    # get memory address
    if isinstance(target, pyvex.IRExpr.Const):
        addr = target.con.value
    else:
        if isinstance(target, pyvex.IRExpr.RdTmp):
            regName = "t"+str(target.tmp)
        elif isinstance(target, pyvex.IRExpr.Get):
            regName = "r"+str(target.offset)
        else:
            print("processStore: memRef type not implemented")
            target.pp()
            sys.exit(-1)
        regNameEnc = regName.encode('utf-8')
#################################
#        supBound = apron.getVariableSupInterval(regNameEnc)
#        infBound = apron.getVariableInfInterval(regNameEnc)
#        if infBound == supBound:
#            addr = int(infBound) % 0x100000000
#################################
        bound = c_int()
        if apron.sameBoundsVariable(regNameEnc, byref(bound)):
            addr = bound.value #% 0x100000000
#################################
        else:
            addr = None
            supBound = apron.getVariableSupInterval(regNameEnc)
            infBound = apron.getVariableInfInterval(regNameEnc)

    # set memory content
    if addr is not None:
        apron.initExpr()
        if isinstance(reg, pyvex.IRExpr.Const):
            apron.addConstToExpr(reg.con.value)
            if __debug__ and DEBUG_MEM:
                print("Addr:", hex(addr), "Const val:", reg.con.value)
        else:
            if isinstance(reg, pyvex.IRExpr.RdTmp):
                regName = "t"+str(reg.tmp)
            elif isinstance(reg, pyvex.IRExpr.Get):
                regName = "r"+str(reg.offset)
            else:
                print("processStore: memRef type not implemented")
                reg.pp()
                sys.exit(-1)
            regNameEnc = regName.encode('utf-8')
            apron.addRegToExpr(1, regNameEnc)
            # TODO specify the size of the stored data
            if __debug__ and DEBUG_MEM:
                print("Addr:", hex(addr), "Val:", regName)
        apron.assignExprToMem(addr)
    else:
        print("store to unknown address in", regName, "[", infBound, ",", supBound, "]")
        #apron.setMemToTop()
        if infBound == float("-inf"):
            apron.setMemToTop(0)
        else:
            #print("Top from", hex((int(infBound))))
            apron.setMemToTop(int(infBound))
        # TODO: set top until supBound
    return addr


def processLoad(target, ty):
    # get memory address
    if isinstance(target, pyvex.IRExpr.Const):
        addr = target.con.value
    else:
        if isinstance(target, pyvex.IRExpr.RdTmp):
            regName = "t"+str(target.tmp)
        elif isinstance(target, pyvex.IRExpr.Get):
            regName = "r"+str(target.offset)
        else:
            print("processLoad: memRef type not implemented")
            target.pp()
            sys.exit(-1)
        regNameEnc = regName.encode('utf-8')
#################################
#        supBound = apron.getVariableSupInterval(regNameEnc)
#        infBound = apron.getVariableInfInterval(regNameEnc)
#        if infBound == supBound:
#            addr = int(infBound) % 0x100000000
#################################
        bound = c_int()
        if apron.sameBoundsVariable(regNameEnc, byref(bound)):
            addr = bound.value
#################################
        else:
            addr = None

    # get memory content
    if addr is not None:
        floataccess = False
        memName = "m"+str(addr)
        memNameEnc = memName.encode('utf-8')
        if not apron.varExists(memNameEnc):
            if ty == 'Ity_I32':
                memval = state.mem[addr].int32_t.resolved
            elif ty == 'Ity_I16':
                memval = state.mem[addr].int16_t.resolved
                print("processLoad warning: assuming abstract value with 0 in the remaining 16 bits of addr", hex(addr))
            elif ty == 'Ity_I8':
                memval = state.mem[addr].int8_t.resolved
                print("processLoad warning: assuming abstract value with 0 in the remaining 23 bits of addr", hex(addr))
            elif ty == 'Ity_F32' or ty == 'Ity_F64':
                #memval = state.mem[addr].float32_t.resolved
                print("processLoad warning: load of float at addr", hex(addr), "not considered")
                floataccess = True
            else:
                print("unknown size in load to", addr)
                print(ty)
                sys.exit()
            #print("State.mem[", addr, "]:", memval)
            if floataccess or memval.uninitialized:
                if __debug__ and DEBUG_MEM:
                    print("Addr:", hex(addr), "Init val: uninitialized or float")
                apron.initExpr()
                apron.addTopToExpr()
                apron.assignExprToMem(addr)
            else:
                constval = state.solver.eval(memval)
                if __debug__ and DEBUG_MEM:
                    print("Addr:", hex(addr), "Init val:", constval)
                apron.initExpr()
                apron.addConstToExpr(constval)
                apron.assignExprToMem(addr)

        # apron.initExpr() # not needed: addMemToExpr includes initExpr
        apron.addMemToExpr(addr)
    else:
        apron.initExpr()
        apron.addTopToExpr()
    return addr


def processLoadNOMEMORY(target, ty):
    apron.initExpr()
    apron.addTopToExpr()
    return None

# original, correct
#def addMemAccess(PC, dest, addr, numbytes, isload, isconditional):
#    """Process a memory access statement"""
#    global MemAccessAlreadySet
#
#    # for main analysis, memory references are collected
#    # for induction analysis, mem refs are discarded (they already exists)
#    if not MemAccessAlreadySet:
#        node = getCFGNode(PC)
#        bbaddr = node.addr
#        loopaddr = getDeepestLoop(node)
#        if addr is None:
#            addr = 0
#        if isinstance(dest, pyvex.IRExpr.RdTmp):
#            regName = "t"+str(dest.tmp)
#            apron.addMemRef(bbaddr, PC, regName.encode('utf-8'), addr, numbytes,
#                isload, isconditional, loopaddr)
#        elif isinstance(dest, pyvex.IRExpr.Get):
#            regName = "r"+str(dest.offset)
#            apron.addMemRef(bbaddr, PC, regName.encode('utf-8'), addr, numbytes,
#                isload, isconditional, loopaddr)
#        elif isinstance(dest, pyvex.IRExpr.Const):
#            apron.addMemConstRef(bbaddr, PC, dest.con.value, numbytes,
#                isload, isconditional)
#        else:
#            print("addMemAccess: memRef type not implemented")
#            dest.pp()
#            sys.exit(-1)

# test
def addMemAccess(CFGNode, PC, dest, addr, numbytes, isload, isconditional):
    """Process a memory access statement"""
    global MemAccessAlreadySet
    global tested_loop

    # for main analysis, memory references are collected
    # for induction analysis, mem refs are discarded (they already exists)
    if not MemAccessAlreadySet:
        #node = get_any_cfg_node(PC)
        node = CFGNode
        bbaddr = node.addr
        loopaddr = getDeepestLoop(node)
        if addr is None:
            addr = 0
        if isinstance(dest, pyvex.IRExpr.RdTmp):
            regName = "t"+str(dest.tmp)
            apron.addMemRef(set_node_id(CFGNode), bbaddr, PC, regName.encode('utf-8'), addr, numbytes,
                isload, isconditional, loopaddr)
        elif isinstance(dest, pyvex.IRExpr.Get):
            regName = "r"+str(dest.offset)
            apron.addMemRef(set_node_id(CFGNode), bbaddr, PC, regName.encode('utf-8'), addr, numbytes,
                isload, isconditional, loopaddr)
        elif isinstance(dest, pyvex.IRExpr.Const):
            apron.addMemConstRef(set_node_id(CFGNode), bbaddr, PC, dest.con.value, numbytes,
                isload, isconditional)
        else:
            print("addMemAccess: memRef type not implemented")
            dest.pp()
            sys.exit(-1)
    else:
        # induction analysis
        #1. localizar regName (descartar casos Const)
        if isinstance(dest, pyvex.IRExpr.RdTmp):
            regName = "t"+str(dest.tmp)
        elif isinstance(dest, pyvex.IRExpr.Get):
            regName = "r"+str(dest.offset)
        else:
            return
        #2. para cada reg, evaluar constraint: regName - shadowReg
        bound = c_int()
        for indvar in registers:
            indvarName = "s" + str(indvar)
            if apron.sameBoundsDifferenceInterval(regName.encode('utf-8'), indvarName.encode('utf-8'), byref(bound)):
            #3. anotar los resultados constantes
                apron.addMemRefOffset(set_node_id(CFGNode), PC, tested_loop, regName.encode('utf-8'), indvar, bound.value)
                # print("Encontrado offset", offset[indvar], "para", indvarName, "en @", hex(PC), regName)


############################ BINARY OPERATIONS ############################
def is_arithmetic_bin_op(data):
    if data.op == 'Iop_Add32' or data.op == 'Iop_Sub32':
        return True
    else:
        return False


def arithmeticBinOp(data):
    """Process an arithmetic (+,-) binary operation"""
    apron.initExpr()

    # TODO: manage overflows!
    if data.op == 'Iop_Add32':
        coef = 1
    elif data.op == 'Iop_Sub32':
        coef = -1
    else:
        print("Unknown arithmetic binary operation:")
        data.pp()
        sys.exit(-1)

    # proceso primer operando
    op0isconstant = False
    if isinstance(data.args[0], pyvex.IRExpr.RdTmp):
        # generar elem expresion "t%d", data.tmp
        regName = "t"+str(data.args[0].tmp)
        apron.addRegToExpr(1, regName.encode('utf-8'))
    elif isinstance(data.args[0], pyvex.IRExpr.Get):
        # generar elem expresion "r%d", data.offset
        regName = "r"+str(data.args[0].offset)
        apron.addRegToExpr(1, regName.encode('utf-8'))
    elif isinstance(data.args[0], pyvex.IRExpr.Const):
        # generar elem expresion
        apron.addConstToExpr(data.args[0].con.value)
        op0isconstant = True
    else:
        print("operando 0 en arithmetic binop no implementado:")
        print(type(data.args[0]))
        sys.exit(-1)

    # proceso segundo operando
    if isinstance(data.args[1], pyvex.IRExpr.RdTmp):
        # generar elem expresion "t%d", data.tmp
        regName = "t"+str(data.args[1].tmp)
        apron.addRegToExpr(coef, regName.encode('utf-8'))
    elif isinstance(data.args[1], pyvex.IRExpr.Get):
        # generar elem expresion "r%d", data.offset
        regName = "r"+str(data.offset)
        apron.addRegToExpr(coef, regName.encode('utf-8'))
    elif isinstance(data.args[1], pyvex.IRExpr.Const):
        # generar elem expresion
        apron.addConstToExpr(coef*data.args[1].con.value)
        if op0isconstant:
            print("Binary operation with two constants:")
            data.pp()
    else:
        print("operando 1 en arithmetic binop no implementado")
        print(type(data.args[1]))
        sys.exit(-1)


def is_geometric_bin_op(data):
    if data.op == 'Iop_Shl32' or data.op == 'Iop_Shr32' or data.op == 'Iop_Sar32' or data.op == 'Iop_MullS32' or data.op == 'Iop_Mul32' or data.op == 'Iop_MullU32':
        return True
    else:
        return False


def geometricBinOp(data):
    """Process a geometric (*,/,sh,sa) binary operation"""
    apron.initExpr()

    if data.op == 'Iop_Shl32':
        # proceso segundo operando
        if isinstance(data.args[1], pyvex.IRExpr.Const):
            coef = 2**data.args[1].con.value
            #print("Shl: verify that shifted value is positive and does not overflow")
            print("Warning: ShiftLeft: assuming that shifted value is positive and does not overflow")
            # TODO:
            # 1. comprobar signo de primer operando
            #  - si es negativo: interpretar como natural
            #  - si desconocido: top
            # 2. comprobar magnitud
            #  - si <2^31: multiplicar por 2 como ahora
            #  - si >2^31: (x*2)-0x100000000
        else:
            print("operando 1 en geometric binop no implementado: Top")
            print(type(data.args[1]))
            apron.addTopToExpr()
            return
            #sys.exit(-1)

        #proceso primer operando
        if isinstance(data.args[0], pyvex.IRExpr.RdTmp):
            # generar elem expresion "t%d", data.tmp
            regName = "t"+str(data.args[0].tmp)
            apron.addRegToExpr(coef, regName.encode('utf-8'))
        elif isinstance(data.args[0], pyvex.IRExpr.Get):
            # generar elem expresion "r%d", data.offset
            regName = "r"+str(data.args[0].offset)
            apron.addRegToExpr(coef, regName.encode('utf-8'))
        else:
            print("operando 0 en geometric binop no implementado")
            print(type(data.args[0]))
            sys.exit(-1)

#    elif data.op == 'Iop_Shr32':
#        # proceso segundo operando
#        if isinstance(data.args[1], pyvex.IRExpr.Const):
#            num = 1
#            denom = 2**data.args[1].con.value
#            print("TODO: verify that shifted value is possitive")
#        else:
#            print("operando 1 en geometric binop no implementado")
#            print(type(data.args[1]))
#            sys.exit(-1)
#
#        #proceso primer operando
#        if isinstance(data.args[0], pyvex.IRExpr.RdTmp):
#            # generar elem expresion "t%d", data.tmp
#            regName = "t"+str(data.args[0].tmp)
#            apron.addRegToArray(num, denom, regName.encode('utf-8'))
#        elif isinstance(data.args[0], pyvex.IRExpr.Get):
#            # generar elem expresion "r%d", data.offset
#            regName = "r"+str(data.args[0].offset)
#            apron.addRegToArray(num, denom, regName.encode('utf-8'))
#        else:
#            print("operando 0 en geometric binop no implementado")
#            print(type(data.args[0]))
#            sys.exit(-1)

#    elif data.op == 'Iop_Sar32': # Arithemtic Shift Right
#        print("Arithmetic Shift Right not considered: Top")
#        apron.addTopToArray()
#    elif data.op == 'Iop_MullS32': # Widening multiply
#        print("MullS not considered: Top")
#        apron.addTopToArray()
#    elif data.op == 'Iop_Mul32':
#        print("Mul32 not considered: Top")
#        apron.addTopToArray()
    else:
        #data.pp()
        print("Geometric binary operation", data.op, "not considered: Top")
        apron.addTopToExpr()



def is_logic_bin_op(data):
    if data.op == 'Iop_Or32' or data.op == 'Iop_And32' or data.op == 'Iop_Xor32':
        return True
    else:
        return False


def is_float_op(data):
    if data.op == 'Iop_CmpF64' or data.op == 'Iop_CmpF32' or data.op == 'Iop_F64toI32S' or data.op == 'Iop_F64toF32' or data.op == 'Iop_SubF32' or data.op == 'Iop_SubF64' or data.op == 'Iop_MulF32' or data.op == 'F32toF64' or data.op == 'Iop_AddF32' or data.op == 'Iop_F64toI32U' or data.op == 'Iop_MulF64' or data.op == 'Iop_AddF64' or data.op == 'Iop_DivF64' or data.op == 'Iop_DivF32':
        # unary: F32toF64
        # binary: Iop_CmpF64 Iop_CmpF32 Iop_F64toI32S Iop_F64toF32 Iop_F64toI32U
        # ternary: Iop_SubF32 Iop_SubF64 Iop_MulF32 Iop_AddF32 Iop_MulF64  Iop_AddF64 Iop_DivF64 Iop_DivF32
        return True
    else:
        return False


def logicBinOp(data):
    """Process a logic (and,or) binary operation"""
    apron.initExpr()
    apron.addTopToExpr()
    data.pp()
    print("Logic operations not considered: Top")


def is_comparison_bin_op(data):
    if data.op == 'Iop_CmpEQ32' or data.op == 'Iop_CmpNE32' or data.op == 'Iop_CmpLE32S' or data.op == 'Iop_CmpLE32U' or data.op == 'Iop_CmpLT32S' or data.op == 'Iop_CmpLT32U':
        return True
    else:
        return False


def comparisonBinOp(data):
    """Process a comparison (==,!=,<,>,<=,>=) binary operation"""
    # TODO: difference signed/unsigned
    apron.initExpr()
    # set type of constraint and coefficients
    if data.op == 'Iop_CmpEQ32':    # x1 == x2:
        apron.setConsEQ()           # x1 - x2 = 0; NEG: x1-x2!=0 or 0!=x2-x1
        coef1 = 1
        coef2 = -1
    elif data.op == 'Iop_CmpNE32':  # x1 != x2:
        apron.setConsDISEQ()        # x1 - x2 != 0; NEG: x1-x2==0 or 0==x2-x1
        coef1 = 1
        coef2 = -1
    elif data.op == 'Iop_CmpLE32S' or data.op == 'Iop_CmpLE32U':
        # x1 <= x2: - x1 + x2 >= 0; NEG: 0<x1-x2
        apron.setConsSUPEQ()
        coef1 = -1
        coef2 = 1
    elif data.op == 'Iop_CmpLT32S' or data.op == 'Iop_CmpLT32U':
        # x1 < x2: - x1 + x2 > 0; NEG: 0<=x1-x2
        apron.setConsSUP()
        coef1 = -1
        coef2 = 1
    else:
        print("Unknown comparison binary operation:")
        data.pp()
        sys.exit(-1)

    # proceso primer operando
    if isinstance(data.args[0], pyvex.IRExpr.RdTmp):
        regName = "t"+str(data.args[0].tmp)
        apron.addRegToExpr(coef1, regName.encode('utf-8'))
    elif isinstance(data.args[0], pyvex.IRExpr.Get):
        regName = "r"+str(data.args[0].offset)
        apron.addRegToExpr(coef1, regName.encode('utf-8'))
    elif isinstance(data.args[0], pyvex.IRExpr.Const):
        apron.addConstToExpr(coef1*data.args[0].con.value)
    else:
        print("operando 0 en comparison binop no implementado:")
        print(type(data.args[0]))
        sys.exit(-1)

    # proceso segundo operando
    if isinstance(data.args[1], pyvex.IRExpr.RdTmp):
        regName = "t"+str(data.args[1].tmp)
        apron.addRegToExpr(coef2, regName.encode('utf-8'))
    elif isinstance(data.args[1], pyvex.IRExpr.Get):
        regName = "r"+str(data.offset)
        apron.addRegToExpr(coef2, regName.encode('utf-8'))
    elif isinstance(data.args[1], pyvex.IRExpr.Const):
        apron.addConstToExpr(coef2*data.args[1].con.value)
    else:
        print("operando 1 en comparison binop no implementado")
        print(type(data.args[1]))
        sys.exit(-1)

    apron.completeCons() # preserves constraint


## Process an irvex expression
def studyExpr(data, currentPC, CFGNode):
    global anyStoreToUnknownAddr, state
#def studyExpr(data, bbaddr, currentPC):
    """Process an irvex expression."""
    global ncomparisons
    # data.pp()
    if isinstance(data, pyvex.IRExpr.Binop):
        if is_arithmetic_bin_op(data):
            arithmeticBinOp(data)
        elif is_comparison_bin_op(data):
            comparisonBinOp(data)
            ncomparisons = ncomparisons + 1
            if ncomparisons > 1:
                print("WARNING: more than 1 comparison in basic block")
                sys.exit(-1)
        elif is_geometric_bin_op(data):
            geometricBinOp(data)
        elif is_logic_bin_op(data):
            logicBinOp(data)
            if ncomparisons > 0:
                ncomparisons = 0
                if __debug__ and DEBUG_COMPARE:
                    print("Warning: comparaciones a 0")
        elif is_float_op(data):
            print("Floating point operations not considered: Top")
            apron.initExpr()
            apron.addTopToExpr()
        elif data.op == 'Iop_32HLto64':
            print("32HLto64 operation not considered: Top")
            apron.initExpr()
            apron.addTopToExpr()
        else:
            data.pp()
            print(data.op)
            print("operación en binop no implementada")
            sys.exit(-1)
    elif isinstance(data, pyvex.IRExpr.Unop):
        if data.op == 'Iop_Not32' or data.op == 'Iop_Not1':
            if ncomparisons > 0:
                ncomparisons = ncomparisons - 1
                if __debug__ and DEBUG_COMPARE:
                    print("Warning: comparación descartada")
        apron.initExpr()
        apron.addTopToExpr()
        if __debug__ and DEBUG_IRVEX:
            data.pp()
            print("  unary op: no impl")
#        # print("  unary op")
#        data.pp()
#        apron.initExpr()
#        if data.op == 'Iop_1Uto32' or data.op == 'Iop_32to1' or data.op == 'Iop_32to8':
#            # 1b to 32b, unsigned; 32b to 1b
#            if isinstance(data.args[0], pyvex.IRExpr.RdTmp):
#                regName = "t"+str(data.args[0].tmp)
#                apron.addRegToArray(1, regName.encode('utf-8'))
#            elif isinstance(data.args[0], pyvex.IRExpr.Get):
#                # generar elem expresion "r%d", data.offset
#                regName = "r"+str(data.args[0].offset)
#                apron.addRegToArray(1, regName.encode('utf-8'))
#            else:
#                data.pp()
#                print("unary operand: no impl")
#                sys.exit(-1)
#        elif data.op == 'Iop_Not32': # 2s complement: not x = -x - 1
#            print("Iop_Not32 not considered: Top")
#            print("MAY REVERSE A CONDITION!!!!")
#            if (ncomparisons > 0):
#                ncomparisons = ncomparisons - 1
#                print("Warning: comparación perdida")
#            apron.addTopToArray()
#        elif data.op == 'Iop_Not1': # not x = x - 1 = x + 1
#            print("Iop_Not1 not considered: Top")
#            print("MAY REVERSE A CONDITION!!!!")
#            if (ncomparisons > 0):
#                ncomparisons = ncomparisons - 1
#                print("Warning: comparación perdida")
#            apron.addTopToArray()
#        elif data.op == 'Iop_64HIto32': # I64 -> I32, high half
#            print("Iop_64HIto32 not considered: Top")
#            apron.addTopToArray()
#        else:
#            data.pp()
#            print("  unary op: no impl")
#            sys.exit(-1)
    elif isinstance(data, pyvex.IRExpr.ITE):
        print("Ternary if-then-else not considered: Top")
        #data.pp()
        #print("  ternary if-then-else: no impl")
#        print("  Ternary If-Then-Else (operación predicada). Si se cumple:")
#        data.cond.pp()
#        print("  entonces:")
#        data.iftrue.pp()
#        print("  Si no:")
#        data.iffalse.pp()
#        print("  Currently implemented as Top")
        apron.initExpr()
        apron.addTopToExpr()
        if ncomparisons > 0:
            ncomparisons = ncomparisons - 1
            if __debug__ and DEBUG_COMPARE:
                print("Warning: comparación perdida")
    elif isinstance(data, pyvex.IRExpr.Triop):
        if is_float_op(data):
            print("Floating point operations not considered")
            apron.initExpr()
            apron.addTopToExpr()
        else:
            print("  ternary op: no impl")
            data.pp()
            sys.exit(-1)
    elif isinstance(data, pyvex.IRExpr.Qop):
        print("  quaternary op: no impl")
        data.pp()
        sys.exit(-1)
    elif isinstance(data, pyvex.IRExpr.RdTmp):
        # print("  lectura reg temp: ", data.tmp)
        apron.initExpr()
        regName = "t"+str(data.tmp)
        apron.addRegToExpr(1, regName.encode('utf-8'))
    elif isinstance(data, pyvex.IRExpr.Const):
        if isinstance(data.con.value, float):
            print("  const: ", data.con)
            print("Floating point operations not considered")
            apron.initExpr()
            apron.addTopToExpr()
        else:
            apron.initExpr()
            apron.addConstToExpr(data.con.value)
    elif isinstance(data, pyvex.IRExpr.Get):
        # print("  registro: ", " ; r"+str(data.offset))
        apron.initExpr()
        regName = "r"+str(data.offset)
        apron.addRegToExpr(1, regName.encode('utf-8'))
    elif isinstance(data, pyvex.IRExpr.GetI):
        print("  registro (bits específicos): no impl")
        sys.exit(-1)
    elif isinstance(data, pyvex.IRExpr.Load):
        #constval = processLoad(data.addr, data.ty)
        addr = processLoad(data.addr, data.ty)
        if __debug__ and DEBUG_MEM:
            #print(" Lectura de mem @", data.addr, ":", constval)
            print(" Lectura de mem @", data.addr)
        if data.ty == 'Ity_I32':
            addMemAccess(CFGNode, currentPC, data.addr, addr, 4, 1, 0)
        elif data.ty == 'Ity_I16':
            addMemAccess(CFGNode, currentPC, data.addr, addr, 2, 1, 0)
        elif data.ty == 'Ity_F64':
            addMemAccess(CFGNode, currentPC, data.addr, addr, 8, 1, 0)
        else:
            print("unknown size in load")
            data.pp()


#        apron.initExpr()
#        if __debug__ and DEBUG_MEM:
#            print(" Lectura de mem @", data.addr, ": Top")
#        if data.ty == 'Ity_I32':
#            addMemAccess(currentPC, data.addr, 4, 1, 0)
#            if anyStoreToUnknownAddr:
#                apron.addTopToExpr()
#            elif isinstance(data.addr, pyvex.IRExpr.Const):
#                memval = state.mem[data.addr.con.value].int32_t.resolved
#                print("Warning: assuming int32_t", memval, "in load from", data.addr)
#                apron.addConstToExpr(state.solver.eval(memval))
#            else:
#                print("Warning: not yet supported load from", data.addr)
#                data.pp()
#                sys.exit()
#        elif data.ty == 'Ity_I16':
#            addMemAccess(currentPC, data.addr, 2, 1, 0)
#            if anyStoreToUnknownAddr:
#                apron.addTopToExpr()
#            else:
#                memval = state.mem[data.addr].int16_t.resolved
#                print("Warning: assuming int16_t (", memval, ") in load from", hex(data.addr))
#                apron.addConstToExpr(memval)
#        else:
#            print("unknown size in load")
#            data.pp()
#            sys.exit()
#        apron.addTopToExpr()
    elif isinstance(data, pyvex.IRExpr.CCall):
        if __debug__ and DEBUG_COMPARE:
            data.pp()
            print("  CCall (calculate_flag/condition) no implementada: Top")
        if ncomparisons != 0:
            if __debug__ and DEBUG_COMPARE:
                print("Number of comparisons in this basic block is", ncomparisons)
            ncomparisons = 0
        apron.initExpr()
        apron.addTopToExpr()
    else:
        data.pp()
        print("  expresión no implementada:", type(data))
        sys.exit(-1)


############################ LOOP/NODE OPERATIONS ############################
def getDeepestLoop(node):
    """get deepest loop including CFGnode node"""
    loopnode = None
    for loop in loopInfo.loops:
        if node.to_codenode() in loop.body_nodes:
            if loopnode is None:
                loopnode = loop
            else:
                if loopnode not in loop.subloops:
                    loopnode = loop
    if loopnode is None:
        return 0
    else:
        return loopnode.entry.addr

def set_node_id(node):
    """must fit in 32 bits"""
    #return c_int(id(node))
    #return id(node) % 0x100000000
    return node.__hash__() % 0x10000000

def set_alternative_id(node):
    """must fit in 32 bits"""
    return ~(node.__hash__() % 0x10000000)

def get_cfgid_node(instaddr, nodeid):
    while (cfg.model.get_any_node(instaddr) is None) and (instaddr > 0):
        instaddr -= 4
    for node in cfg.model.get_all_nodes(instaddr):
        if set_node_id(node) == nodeid:
            return node
    print("get_cfgid_node: no node found for instr. at", hex(instaddr), "and nodeId ", nodeid)
    sys.exit(-1)
    #return None

def get_any_cfg_node(instaddr):
    while (cfg.model.get_any_node(instaddr) is None) and (instaddr > 0):
        instaddr -= 4
    return cfg.model.get_any_node(instaddr)


def getLoopSuccessor(CFGNode):
    for loop in loopInfo.loops:
        if CFGNode.addr == loop.entry.addr:
            return cfg.model.get_any_node(loop.break_edges[0][1].addr)
    return False


def isLoopHeader(CFGNode):
    for loop in loopInfo.loops:
        if CFGNode.addr == loop.entry.addr:
            return True
    return False


def getLoop(nodeaddr):
    for loop in loopInfo.loops:
        if nodeaddr == loop.entry.addr:
            return loop
    return None


## Test whether there is a back-edge from previous to CFGNode (loop header)
def isBackEdge(CFGNode, prevNode):
    for loop in loopInfo.loops:
        if CFGNode.addr == loop.entry.addr:
            if (prevNode.to_codenode(), CFGNode.to_codenode()) in loop.continue_edges:
                return True
    return False


#def more_predecessors_than_visits(CFGNode, numvisits):
#    numpred = len(CFGNode.predecessors)
#    for pred in CFGNode.predecessors:
#        print("name:", pred._name, "; addr:", hex(pred.addr))
#        if isBackEdge(CFGNode, pred):
#            numpred -= 1
#        elif pred._name is None:
#            numpred -= 1
#    if numpred - 1 <= numvisits:
#        return False
#    else:
#        return True
#
#def printPath(path):
#    for node in path:
#        print(" ", hex(node.addr))



############################ EXPLORE CFG ############################

## Process the CFG. Initially, call as exploreLoop2(first,last,first,first)
#  In-depth recursive CFG processing.
#
#  @param[in] first First CFG node to process
#  @param[in] last CFG node to stop (last not processed)
#  @param[in] CFGNode current CFG node to process
#  @param[in] previous previously processed CFG node
#def exploreLoop2(first, last, CFGNode, previous):
def exploreLoop2(first, nodeset, CFGNode, previous):
    global visits
    global numcfgnodes

    if __debug__ and DEBUG_FIXPOINT:
        print("New CFG Node:", hex(CFGNode.addr), ", previous:", hex(previous.addr))
        #print("successors", CFGNode.successors)
    #if CFGNode == last:
    if CFGNode not in nodeset:
        if __debug__ and DEBUG_FIXPOINT:
            #print("last node reached")
            print("nuevo bloque fuera de bloques válidos: no se analiza")
        return
    if CFGNode in visits:
        if __debug__ and DEBUG_FIXPOINT:
            print(hex(CFGNode.addr), "had", visits[CFGNode], "visits")
        visits[CFGNode] += 1
        if isBackEdge(CFGNode, previous):
            # TODO recover limits / condition as array
            if __debug__ and DEBUG_FIXPOINT:
                print("widening: node.IN <- node.prev.IN \/ (node.prev.IN U node.IN)")
            if apron.wideningStatesNeedsUpdate(set_node_id(CFGNode)):
                pass
            else:
                if __debug__ and DEBUG_FIXPOINT:
                    print("widen: no changes")
                return # no changes; no further processing needed
        else:
            if __debug__ and DEBUG_FIXPOINT:
                print("join: node.in <- node.in JOIN prev.out")
            if apron.joinStatesNeedsUpdate(set_node_id(CFGNode)):
                pass
            else:
                if __debug__ and DEBUG_FIXPOINT:
                    print("join: no changes")
                return # no changes; no further processing needed
    else: # not yet visited
        if __debug__ and DEBUG_FIXPOINT:
            print("nuevo bloque, id", set_node_id(CFGNode))
            print(CFGNode)
        apron.newNodeState(set_node_id(CFGNode), CFGNode.addr) # set IN state
        if CFGNode != first:
            visits[CFGNode] = 1
        else:
            visits[CFGNode] = 99999
#       if "+0x" not in CFGNode.name:
#           numcfgnodes += len(cfg.kb.functions.function(name=CFGNode.name).nodes)
        print(len(visits), "("+hex(CFGNode.addr)+") of", numcfgnodes, "nodes visited")

    # analizar
    if __debug__ and DEBUG_FIXPOINT:
        print("analizar bloque (generar node.out):")
        print("")
    numcmps = generateOutState(CFGNode)
    if __debug__ and DEBUG_FIXPOINT:
        print("")
    apron.setOutState(set_node_id(CFGNode))
    if len(CFGNode.successors) == 0: # exit: no more nodes
        if __debug__ and DEBUG_FIXPOINT:
            print("no more nodes")
        return
    else: # len(CFGNode.successors)>1
        for childNode in CFGNode.successors:
            #unconditional = False
            if __debug__ and DEBUG_FIXPOINT:
                print("exploring successor", hex(childNode.addr), "from", hex(CFGNode.addr), set_node_id(CFGNode))
            apron.setDefaultState(set_node_id(CFGNode))
            # get branch condition, if any
            #irsb = proj.factory.block(CFGNode.addr).vex
            irsb = CFGNode.block.vex
            branch = irsb.statements[len(irsb.statements)-1]
            if isinstance(branch, pyvex.IRStmt.Exit):
                if __debug__ and DEBUG_FIXPOINT:
                    branch.pp()
                if branch.dst.value == childNode.addr:
                    if __debug__ and DEBUG_FIXPOINT:
                        print(" taken branch")
                    if numcmps > 0:
                        notbottom = apron.addConstraint()
                    else:
                        notbottom = True
                        if __debug__ and DEBUG_COMPARE:
                            print("Warning: condition lacking!")
                else:
                    if __debug__ and DEBUG_FIXPOINT:
                        print(" not taken branch")
                    if numcmps > 0:
                        notbottom = apron.addNegConstraint()
                    else:
                        notbottom = True
                        if __debug__ and DEBUG_COMPARE:
                            print("Warning: condition lacking!")
            else:
                if __debug__ and DEBUG_FIXPOINT:
                    print(" no conditional branch")
                #unconditional = True
                notbottom = True
            if notbottom:
                #exploreLoop2(first, last, childNode, CFGNode)
                exploreLoop2(first, nodeset, childNode, CFGNode)
                #if unconditional:
                #    return
            else:
                if __debug__ and DEBUG_FIXPOINT:
                    print("unfeasible path")
        return


## Process loop CFG to test whether indreg is an induction variable
#
#  @param[in] loop Loop to explore for induction variable
#  @param[in] indreg register to test whether it is induction var.
#  @param[in] cfgnode current CFG node to process
def induction_explore(loop, cfgnode, previous, nodeset, loopCFGNode):
    global alreadyExplored

    if __debug__ and DEBUG_FIXPOINT_INDVARS:
        print("New CFG Node:", hex(cfgnode.addr), ", previous:", hex(previous.addr))
    if cfgnode.addr not in nodeset:
        print("Node", hex(cfgnode.addr), "outside nodeset of loop", hex(loop.entry.addr))
        return
    if cfgnode in alreadyExplored:
        if __debug__ and DEBUG_FIXPOINT_INDVARS:
            print("already explored")
        if isBackEdge(cfgnode, previous):
            if apron.wideningStatesNeedsUpdate(set_alternative_id(cfgnode)):
                pass
            else:
                if __debug__ and DEBUG_FIXPOINT_INDVARS:
                    print("return from widen")
                return
        else:
            if apron.joinStatesNeedsUpdate(set_alternative_id(cfgnode)):
                pass
            else:
                if __debug__ and DEBUG_FIXPOINT_INDVARS:
                    print("return from join")
                return
    else:
        if __debug__ and DEBUG_FIXPOINT_INDVARS:
            print("new node:", hex(cfgnode.addr))
        apron.newNodeState(set_alternative_id(cfgnode), -cfgnode.addr)
        alreadyExplored.append(cfgnode)

    numcmps = generateOutState(cfgnode)
    apron.setOutState(set_alternative_id(cfgnode))

    if len(cfgnode.successors) == 0: # exit: no more nodes
        if __debug__ and DEBUG_FIXPOINT_INDVARS:
            print("no more nodes")
        return
    else: # len(CFGNode.successors)>1
        #print("for", cfgnode.successors)
        for childNode in cfgnode.successors:

            #if childNode.to_codenode() in loop.body_nodes:
            # WRONG! calls inside loops are not body nodes
            if (cfgnode.to_codenode(), childNode.to_codenode()) not in loop.break_edges:
                apron.setDefaultState(set_alternative_id(cfgnode))
                # get branch condition, if any
                irsb = cfgnode.block.vex
                branch = irsb.statements[len(irsb.statements)-1]
                if isinstance(branch, pyvex.IRStmt.Exit):
                    if __debug__ and DEBUG_FIXPOINT_INDVARS:
                        branch.pp()
                    if branch.dst.value == childNode.addr:
                        if __debug__ and DEBUG_FIXPOINT_INDVARS:
                            print(" taken branch")
                        if numcmps > 0:
                            notbottom = apron.addConstraint()
                        else:
                            notbottom = True
                            if __debug__ and DEBUG_COMPARE:
                                print("Warning: condition lacking!")
                    else:
                        if __debug__ and DEBUG_FIXPOINT_INDVARS:
                            print(" not taken branch")
                        if numcmps > 0:
                            notbottom = apron.addNegConstraint()
                        else:
                            notbottom = True
                            if __debug__ and DEBUG_COMPARE:
                                print("Warning: condition lacking!")
                else:
                    if __debug__ and DEBUG_FIXPOINT_INDVARS:
                        print(" no conditional branch")
                    notbottom = True
                if notbottom:
                    if childNode.addr == loop.entry.addr:
                        if __debug__ and DEBUG_FIXPOINT_INDVARS:
                            print("header reached")
                        indvars.collectStepInfo(loopCFGNode, loop.entry.addr, registers, reginreg)
                    else:
                        induction_explore(loop, childNode, cfgnode, nodeset, loopCFGNode)
                else:
                    if __debug__ and DEBUG_FIXPOINT_INDVARS:
                        print("unfeasible path")
            # else is break edge
            else:
                if __debug__ and DEBUG_FIXPOINT_INDVARS:
                    print("break edge")
#            else:
#                if __debug__ and DEBUG_FIXPOINT_INDVARS:
#                    print("not analyzing node outside", nodeset)
        if __debug__ and DEBUG_FIXPOINT_INDVARS:
            print("no more nodes")
        return


#def induction_prepare_and_explore(loop):
#    global alreadyExplored
#    global reginreg, regintemp
#
#    #print(hex(loop.entry.addr))
#    reginreg = {}
#    regintemp = {}
#
#    headernode = cfg.model.get_any_node(loop.entry.addr)
#    headerset = False
#    for pred in headernode.predecessors:
#        if pred.to_codenode() not in loop.body_nodes:
#            #print("Pred node:", hex(pred.addr))
#            if apron.nodeExists(pred.addr):
#                apron.setDefaultState(pred.addr)
#                if headerset:
#                    apron.newNodeState(-headernode.addr)
#                    apron.joinStatesNeedsUpdate(-headernode.addr)
#                    apron.setOutState(-headernode.addr) # only to clear them
#                for reg in registers:
#                    indvars.collectBaseInfo(headernode.addr, reg)
#            else:
#                print("not analyzing loop from unreachable block", pred.addr)
#    if apron.nodeExists(-headernode.addr):
#        alreadyExplored = []
#        for reg in registers:
#            apron.setShadowReg(reg)
#        loopnodeset = getLoopNodeSet(loop)
#        #print("Loop node set:", loopnodeset)
#        induction_explore(loop, headernode, headernode, loopnodeset)
#        #print("Assigned registers:", reginreg)
#        indvars.setAssignments(loop.entry.addr, reginreg)
#        #print("clearing negative nodes...")
#        apron.clearNegativeNodes()
#        #print("negative nodes cleared")
#    else:
#        print("not analyzing unreachable loop", hex(headernode.addr))
def induction_prepare_and_explore(loop):
    global alreadyExplored
    global reginreg, regintemp

    #headernode = cfg.model.get_any_node(loop.entry.addr)
    for headernode in cfg.model.get_all_nodes(loop.entry.addr):
        print("Analyzing loop", hex(loop.entry.addr))
        reginreg = {}
        regintemp = {}

        headerset = False
        for pred in headernode.predecessors:
            if pred.to_codenode() not in loop.body_nodes:
                #print("Pred node:", hex(pred.addr))
                if apron.nodeExists(set_node_id(pred), pred.addr):
                    apron.setDefaultState(set_node_id(pred))
                    if not headerset:
                        apron.newNodeState(set_alternative_id(headernode),-headernode.addr)
                        headerset = True
                    apron.joinStatesNeedsUpdate(set_alternative_id(headernode))
                    apron.setOutState(set_alternative_id(headernode)) # OUT = joined INs
                else:
                    print("not analyzing loop from unreachable block", pred.addr)
        if apron.nodeExists(set_alternative_id(headernode), -headernode.addr):
            apron.setDefaultState(set_alternative_id(headernode)) # OUT=joined IN(s)
            for reg in registers:
                #indvars.collectBaseInfo(headernode.addr, reg)
                apron.setShadowReg(reg)
            alreadyExplored = []
            loopnodeset = getLoopNodeSet(loop)
            actualloopnodeset = []
            for addr in loopnodeset:
                if addr in actualnodeset:
                    actualloopnodeset.append(addr)
            loopnodeset = actualloopnodeset
            #print("Loop node set:", loopnodeset)
            induction_explore(loop, headernode, headernode, loopnodeset, headernode)
            #print("Assigned registers:", reginreg)
            #indvars.setAssignments(loop.entry.addr, reginreg)
            #print("clearing negative nodes...")
            apron.clearNegativeNodes()
            #print("negative nodes cleared")
        else:
            print("not analyzing unreachable loop", hex(headernode.addr))


def find_induction_vars():
    global tested_loop
    for loop in loopInfo.loops:
#        if apron.nodeExists(id(loop.entry), loop.entry.addr):
#            if __debug__ and DEBUG_FIXPOINT:
#                print("exploring loop", hex(loop.entry.addr))
#            tested_loop = loop.entry.addr
#            induction_prepare_and_explore(loop)
        tested_loop = loop.entry.addr
        induction_prepare_and_explore(loop)
#    # Validate assignments
#    #print("validating...")
#    for loop in loopInfo.loops:
#        #print(hex(loop.entry.addr))
#        for subloop in loop.subloops:
#            #print("enclosing", hex(subloop.entry.addr))
#            for reg in registers:
#                if loop.entry.addr in indvars.assignments:
#                    assignedlist = indvars.assignments[loop.entry.addr]
#                    #print(assignedlist)
#                    if reg in assignedlist:
#                        assignedreg = assignedlist[reg]
#                        indvars.validateAssignment(subloop.entry.addr, reg, assignedreg)


def dominates(idom, start, i1, i2):
    if apron.memRefIsConditional(i1):
        return False
    else:
        PC1 = apron.getMemRefPC(i1)
        PC2 = apron.getMemRefPC(i2)
        nodeid1 = apron.getMemRefCFGid(i1)
        nodeid2 = apron.getMemRefCFGid(i2)
        node1 = get_cfgid_node(PC1, nodeid1)
        node2 = get_cfgid_node(PC2, nodeid2)
#        node1 = get_any_cfg_node(PC1)
#        node2 = get_any_cfg_node(PC2)
        if node1 == node2:
            if PC1 == PC2:
                return bool(apron.orderedMemRefs(i1, i2))
#                if apron.orderedMemRefs(i1, i2):
#                    return True
#                else:
#                    return False
            elif PC1 < PC2:
                return True
        else:
            return node_dominates(idom, start, node1, node2)

def node_dominates(idom, start, n1, n2): # n1 dominates n2?
    if idom[n2] == n1:
        return True
    elif idom[n2] == start:
        return False
    else:
        return node_dominates(idom, start, n1, idom[n2])

def calcGroupReuse(cfg, start, linesize):
    #idom = networkx.immediate_dominators(cfg.graph, start)

    i1 = 0
    PC1 = apron.getMemRefPC(i1)
    while PC1 != 0:
        i2 = i1 + 1
        PC2 = apron.getMemRefPC(i2)
        while PC2 != 0:
            # TODO descartar predicadas como dominantes? No si es la misma cond! Guardarla?
            if apron.haveGroupReuse(i1, i2, linesize):
                if dominates(idom, start, i1, i2): # i1 dominates i2
                    if ((apron.getLastGR(i2) == 0) or
                        dominates(idom, start, apron.getIndexMemRefLast(i2), i1)):
                        # n2.prevdom doms n1
                        apron.setLastGR(i2, i1)
                elif dominates(idom, start, i2, i1): # if i2 dominates i1
                    if ((apron.getLastGR(i1) == 0) or
                        dominates(idom, start, apron.getIndexMemRefLast(i1), i2)):
                        # n1.prevdom doms n2
                        apron.setLastGR(i1, i2)
            i2 += 1
            PC2 = apron.getMemRefPC(i2)
        i1 += 1
        PC1 = apron.getMemRefPC(i1)


## OLD! gets an upper bound on the number of nodes in a function and its called functions
## OLD! if the same function is called several times, its nodes are accounted several times
## gets the number of nodes in a function and its called functions
#accounted = []
##def getNumNodes(func, accounted=[]):
#def getNumNodes(func):
#    global accounted
#    accounted.append(func.addr)
#    numNodes = len(func.nodes) - len(list(func.get_call_sites()))
#    #print("adding", numNodes, "nodes of", func.name, ":", func.nodes)
#    #for call in list(func.get_call_sites()):
#    for call in set(func.get_call_sites()):
#        calledf = func.get_call_target(call)
#        #if calledf not in accounted:
#        if not (calledf in accounted):
#            #numNodes += getNumNodes(cfg.kb.functions[calledf], accounted)
#            numNodes += getNumNodes(cfg.kb.functions[calledf])
#    return numNodes
#
#
## compares the visited nodes with the counted nodes
## visits is a dictionary of nodes with their number of visits
## accounted is a list of addresses of functions accounted
#def compareNodes(visits, accounted):
#    visitedAddrs = []
#    for node in visits:
#        visitedAddrs.append(node.addr)
#    #print(visits)
#    print(visitedAddrs)
#    print(len(set(visitedAddrs)), "visited")
#    #print(accounted)
#    countednodes = []
#    for addr in accounted:
#        func = cfg.kb.functions[addr]
#        countednodes += list(func.nodes)
#    #print(countednodes)
#    countedAddrs = []
#    for codenode in countednodes:
#        countedAddrs.append(codenode.addr)
#    print(set(countedAddrs))
#    print(len(set(countedAddrs)), "counted")
#    for addr in countedAddrs:
#        if not (addr in visitedAddrs):
#            print(hex(addr), "counted but not visited")
#    for addr in visitedAddrs:
#        if not (addr in countedAddrs):
#            print(hex(addr), "visited but not counted")

#def getNodeSet(func):
#    #print("nodes of", func.name, ":", func.nodes)
#    for node in set(func.nodes):
#        if node.addr not in getNodeSet.nodeset:
#            getNodeSet.nodeset.append(node.addr)
#    for call in set(func.get_call_sites()):
#        calledf = func.get_call_target(call)
#        if calledf != func.addr: # do not follow recursive calls
#            getNodeSet(cfg.kb.functions[calledf])
#    return getNodeSet.nodeset
#getNodeSet.nodeset = []
def get_node_set(func):
    #print("nodes of", func.name, ":", func.nodes)
    for node in set(func.nodes):
        if node not in get_node_set.nodeset:
            get_node_set.nodeset.append(node)
    for call in set(func.get_call_sites()):
        calledf = func.get_call_target(call)
        if calledf != func.addr: # do not follow recursive calls
            get_node_set(cfg.kb.functions[calledf])
    return get_node_set.nodeset
get_node_set.nodeset = []


def getLoopNodeSet(loop):
    loopnodeset = []
    for blocknode in loop.body_nodes:
        loopnodeset.append(blocknode.addr)
    if loop.has_calls:
        for blocknode in loop.body_nodes:
            #print(blocknode.addr)
            node = cfg.model.get_any_node(blocknode.addr)
            #for successor in blocknode.successors():
            # bug? blocknode.successors() fails sometimes
            for successor in node.successors:
                func = cfg.kb.functions.function(addr=successor.addr)
                if func:
                    #loopnodeset += getNodeSet(func)
                    for node2 in get_node_set(func):
                        loopnodeset.append(node2.addr)
    return loopnodeset


# compares the visited nodes with the counted nodes
# visits is a dictionary of nodes with their number of visits
# nodeset is a list of addresses of nodes accounted
#def compareNodes(visits, nodeset):
#    visitedAddrs = []
#    for node in visits:
#        visitedAddrs.append(node.addr)
#    #print(visits)
#    #print(visitedAddrs)
#    #print(len(set(visitedAddrs)), "visited")
#    #print(accounted)
#    #print(len(nodeset), "counted")
#    for addr in nodeset:
#        if addr not in visitedAddrs:
#            print(hex(addr), "counted but not visited")
#    for addr in visitedAddrs:
#        if addr not in nodeset:
#            print(hex(addr), "visited but not counted")
#    return visitedAddrs
def compareNodes(visits, nodeset):
#    for codenode in nodeset:
#        found = False
#        for node in visits:
#            if node.to_codenode() == codenode:
#                found = True
#                break
#        #if node not in visits:
#        if not found:
#            print(hex(codenode.addr), "counted but not visited")
#            # several nodes may have the same addr
    for node in nodeset:
        if node not in visits:
            print(hex(node.addr), "counted but not visited")
    for node in visits:
        if node not in nodeset:
            print(hex(node.addr), "visited but not counted")
            # several nodes may have the same addr
    visitedAddrs = []
    for node in visits:
        visitedAddrs.append(node.addr)
    return visitedAddrs


# get the function including addr and other functions calling function
#def getEnclosingFunctionList(addr, func):
#    if addr in func.block_addrs:
#        return [addr]
#    else:
#        callerlist = []
#        for call in list(func.get_call_sites()):
#            calledf = func.get_call_target(call)
#            callerlist = getEnclosingFunctionList(addr, cfg.kb.functions[calledf])
#            if len(callerlist) != 0:
#                callerlist.append(call)
#        return callerlist
#def getEnclosingFunctionList(addr, func):
#    if addr in func.block_addrs:
#        return [addr]
#    else:
#        callerlist = []
#        for call in list(func.get_call_sites()):
#            calledf = func.get_call_target(call)
#            if func != cfg.kb.functions[calledf]:
#                callerlist = getEnclosingFunctionList(addr, cfg.kb.functions[calledf])
#                if len(callerlist) != 0:
#                    callerlist.append(call)
#        return callerlist
def get_enclosing_call_list(node, func): # returns list of nodes
    if node.to_codenode() in set(func.nodes):
        return [node]
    else:
        callerlist = []
        for call in set(func.get_call_sites()): # for calls in func
            calledf = func.get_call_target(call) # called function
            if func != cfg.kb.functions[calledf]: # if not recursive
                callerlistaux = []
                callerlistaux = get_enclosing_call_list(node, cfg.kb.functions[calledf]) # calls in called func
                if len(callerlistaux) != 0: # some call found
                    # callerlist.append(call)
                    #print("call:", call)
                    domnode = get_dominant_node(call, node)
                    if domnode:
                        callerlistaux.append(domnode)
                        callerlist += callerlistaux
    return callerlist

def get_dominant_node(addr, dominatedNode):
    while (cfg.model.get_any_node(addr) is None) and (addr > 0):
        addr -= 4
    for node in cfg.model.get_all_nodes(addr):
        if node_dominates(IDOM, IDOMSTART, node, dominatedNode):
            return node
    #print("get_dominant_node: No dominant node found in addr", hex(addr), "for node", dominatedNode)
    #print("candidates:", cfg.model.get_all_nodes(addr))

def enclosingLoop(loopaddr):
    encl = None
    deeperloop = getLoop(loopaddr)
    for loop in loopInfo.loops: # for all loops
        if deeperloop in loop.subloops:
            if (encl is None) or (loop in encl.subloops):
                encl = loop
    if encl is None:
        return -1
    else:
        return encl.entry.addr


def genReuseTheoryVectors(idom, start, loopindvars):
    i = 0
    PC = apron.getMemRefPC(i)
    CFGid = apron.getMemRefCFGid(i)
    while PC != 0: # for all memory references
        #print("memRef:", i)
        apron.resetNestInfo()
        addrlist = get_enclosing_call_list(get_cfgid_node(PC, CFGid), fmain)
        #addrlist = get_enclosing_call_list(PC, fmain)
        if len(addrlist) == 0:
            #addrlist = [PC]
            addrlist = [get_cfgid_node(PC, CFGid)]
        #print("caller list:", addrlist)
        # get codenode, its function entry, and any.

        for (loopCFGNode, loopaddr, reg) in loopindvars: # for all induction variables
            for loop in loopInfo.loops: # for all loops
                if loopaddr == loop.entry.addr: # induction detected for this loop
                    for headernode in cfg.model.get_all_nodes(loop.entry.addr):
                        if loopCFGNode == headernode:
#                            for addr in addrlist: # for all possibilities of this memref
#                                node = get_any_cfg_node(addr)
#                                codenode = node.to_codenode()
                            for node in addrlist:
                                #if node.to_codenode() in loop.body_nodes: # if memref is inside loop
                                if node_dominates(idom, start, headernode, node): # if memref is inside loop
                                    thisloopinfo = loopindvars[(loopCFGNode, loopaddr, reg)]
                                    if (thisloopinfo.valid == True) and (thisloopinfo.step != 0):
                                        if thisloopinfo.constantBase == True:
                                            apron.addNestInfo(CFGid, loopaddr, reg, thisloopinfo.step, thisloopinfo.base, 1)
                                        else:
                                            apron.addNestInfo(CFGid, loopaddr, reg, thisloopinfo.step, 0, 0)
                            break
                    break
        apron.genReuseTheoryMemRef(i)

        i += 1
        PC = apron.getMemRefPC(i)
        CFGid = apron.getMemRefCFGid(i)


# returns the number of executions of addr (PC) in its function
#def getExecsInFunction(addr):
#    execs = 1
#    node = get_any_cfg_node(addr)
#    codenode = node.to_codenode()
#    for loop in loopInfo.loops: # for all loops
#        if codenode in loop.body_nodes: # if addr is inside loop
#            #execs *= 20 #maxiters[loop]
#            execs *= flowfacts.maxLoopIter[loop.entry.addr]
#    return execs
#def get_execs_in_function(node):
#    execs = 1
#    for loop in loopInfo.loops: # for all loops
#        for headernode in cfg.model.get_all_nodes(loop.entry.addr):
#            if node.to_codenode() in loop.body_nodes and node_dominates(IDOM, IDOMSTART, headernode, node):
#            # if addr is inside loop and ...dominated by instance of header
#                execs *= flowfacts.maxLoopIter[loop.entry.addr]
#                break
#    return execs
def get_execs_in_function(node):
    execs = 1
    for loop in loopInfo.loops: # for all loops
        if node.to_codenode() in loop.body_nodes: # if it is in loop
            execs *= flowfacts.maxLoopIter[loop.entry.addr]
    return execs


#def getEnclosingNestedFunctionList(nodeaddr, func):
#    if nodeaddr in func.block_addrs:
#        return [nodeaddr]
#    else:
#        callerlist = []
#        for call in list(func.get_call_sites()):
#            calledf = func.get_call_target(call)
#            callAddr = func.get_call_return(call) - 4
#            nesttuple = getEnclosingNestedFunctionList(nodeaddr, cfg.kb.functions[calledf])
#            if len(nesttuple) != 0:
#                calltuple = (
#                callerlist.append([callAddr])
#                callerlist.append([callAddr])
#        if len(callerlist) != 0:
#            return [callerlist]
#        else:
#            return callerlist

#def calcMaxExecs(node):
#    execs = get_execs_in_function(node)
#    maxexecs = get_recursive_execs(execs, node, fmain)
#    return maxexecs

#def getRecursiveExecs(execs, nodeaddr, func):
#    if nodeaddr in func.block_addrs:
#        if __debug__ and DEBUG_COST:
#            print(execs, "executions in", func.name)
#        return execs
#    else:
#        maxexecs = 0
#        callers = []
#        for call in list(func.get_call_sites()):
#            calledf = func.get_call_target(call)
#            if func != cfg.kb.functions[calledf]:
#                callAddr = func.get_call_return(call) - 4
#                if not (callAddr in callers):
#                    callers.append(callAddr)
#                    incallexecs = getRecursiveExecs(execs, nodeaddr, cfg.kb.functions[calledf])
#                    if incallexecs != 0:
#                        outcallexecs = getExecsInFunction(callAddr)
#                        maxexecs += incallexecs * outcallexecs
#                        if __debug__ and DEBUG_COST:
#                            print(incallexecs, "executions from", hex(callAddr), "to", cfg.kb.functions[calledf].name, "(", outcallexecs, ") in", func.name)
#            else:
#                print("recursive call (not supported) detected:", func.name)
#                sys.exit(-1)
##            else:
##                # angr BUG? why repeats calls
##                print("already processed?!")
#        if __debug__ and DEBUG_COST:
#            if maxexecs != 0:
#                print(maxexecs, "executions in", func.name)
#        return maxexecs
def get_recursive_execs(node, func):
    if node.to_codenode() in set(func.nodes):
        execs = get_execs_in_function(node)
        if __debug__ and DEBUG_COST:
            print("   ", execs, "executions in", func.name)
        return execs
    else: # node not in func
        maxexecs = 0
        callers = []
        for call in list(func.get_call_sites()): # for calls in func
            calledf = func.get_call_target(call) # called address
            if func != cfg.kb.functions[calledf]: # if not recursive
                if not (call in callers): # if not yet processed
                    callers.append(call)
                    if node.addr == calledf:
                        domnode = node
                    else:
                        domnode = get_dominant_node(calledf, node)
                    # if called function addr is dominant
                    if domnode:
                        incallexecs = get_recursive_execs(node, cfg.kb.functions[calledf])
                        if incallexecs != 0:
                            outcallexecs = get_execs_in_function(get_any_cfg_node(call))
                            maxexecs += incallexecs * outcallexecs
                            if __debug__ and DEBUG_COST:
                                print("   ", incallexecs, "executions from", hex(call), "to", cfg.kb.functions[calledf].name, "(", outcallexecs, ") in", func.name)
                            # TODO: if CFGEmulated, break
#                    else: # not dominant instance
#                        if __debug__ and DEBUG_COST:
#                            print("   ", cfg.kb.functions[calledf].name, "not dominant from", hex(call), "(discarding previous numbers)")
            else:
                print("recursive call (not supported) detected:", func.name)
                sys.exit(-1)
        if __debug__ and DEBUG_COST:
            if maxexecs != 0:
                print("   Total:", maxexecs, "executions in", func.name)
        return maxexecs


#def calcMaxExecs(leafexecs, calltree):
#    branches = len(calltree)
#    if branches == 0:
#        printf("not called??!!")
#    elif branches == 1:
#        printf("called from main")
#        return getExecsInFunction(calltree[0])
#    else:
#        execs = 0
#        i = 0
#        while i < (branches-1):
#            execs += calcMaxExecs(calltree[i])
#            i += 1
#    for branch in calltree:
#        if len(branch) == 1: # leaf node
#            branchexecs += leafexecs
#        else:
#            branchexecs

#def getEnclosingFunction(addr):
#    node = getCFGNode(addr)
#    if node.addr in fmain.block_addrs:
#        return None
#    else:
#        for call in list(fmain.get_call_sites()):
#            calledf = func.get_call_target(call)
#            callerlist = getEnclosingFunctionList(addr, cfg.kb.functions[calledf])
#            if len(callerlist) != 0:
#                callerlist.append(call)
#        return callerlist

def setMaxExecs():
    i = 0
    PC = apron.getMemRefPC(i)
    while PC != 0: # for all memory references
        nodeid = apron.getMemRefCFGid(i)
        node = get_cfgid_node(PC, nodeid)
        if __debug__ and DEBUG_COST:
            print("%d)" % i, hex(PC), node, nodeid)
        #execs = calcMaxExecs(node)
        execs = get_recursive_execs(node, fmain)
        #print(hex(PC), "/", PC, "execs in total:", execs)
        apron.addCount(i, execs)
        i += 1
        PC = apron.getMemRefPC(i)

#def classify():
#    i = 0
#    PC = apron.getMemRefPC(i)
#    while PC != 0: # for all memory references
#
#        i += 1
#        PC = apron.getMemRefPC(i)

def calcGRDistance(idom, start, last):
    dist = 0
    lastPC = apron.getMemRefPC(last)
    first = apron.getIndexMemRefLast(last)
    firstPC = apron.getMemRefPC(first)
    i = 0
    PC = apron.getMemRefPC(i)
    while PC != 0: # for all memory references
        if i != last and i != first:
            if dominates(idom, start, i, last) and dominates(idom, start, first, i):
                dist += 1 # TODO: OJO, si está en un bucle, sumar tantas líneas como toque
                # TODO: OJO, varios accesos a la misma dirección (GR) no incrementan
                # TODO: OJO, accesos a otros conjuntos no incrementan
        i += 1
        PC = apron.getMemRefPC(i)
    return dist

#def calcSelfTempDistance(i):
#    loopaddr = apron.getTempLoop(i) # bucle más externo con coef=0
#    if loopaddr != 0:
#        # recorrer bloques y contar ld/st de camino con más ld/st
#        loop = loopInfo.loops[entry.addr=loopaddr]
#
#    else:
#        return 0

def calcDistances(idom, start):
    i = 0
    PC = apron.getMemRefPC(i)
    while PC != 0: # for all memory references
        #if apron.hasGroupReuse(i): # LRU only, not needed for ACDC
        if apron.getLastGR(i): # LRU only, not needed for ACDC
            grdist = calcGRDistance(idom, start, i)
            print("GR distance of", i, "is", grdist)
#        elif apron.hasSelfReuse(i):
#            calcTempDistance(i) # scalar and array
#            # TODO: ojo! podemos tener varios bucles internos con temporal, pero con distintas distancias. Para LRU podríamos tener hits/misses solo a veces. Asumir distancia máxima por simplificar?
#            calcSpaDistance(i) # array only
        i += 1
        PC = apron.getMemRefPC(i)

#        execsInFunc = getExecsInFunction(PC)
#        print(hex(PC), "/", PC, "execs in funct:", execsInFunc)
## 3: para cada punto de llamada, hacer #1
#        execs = 1
#        node = getCFGNode(PC)
#        addrlist = getEnclosingNestedFunctionList(execsInFunc, node.addr, fmain)
#        print(hex(PC), "/", PC, "execs in total:", addrlist)
#        #print(addrlist)

#        previous = fmain
#        for addr in addrlist: # for all possibilities of this memref
#            if addr in previous.get_call_sites():
#                execs *= getExecsInFunction(addr)
#            else:
#                execs +=
## 2: ver puntos de llamada a la función que contiene el acceso
#        if len(addrlist) == 0:
#            addrlist = [PC]
#        for loop in loopInfo.loops: # for all loops

#            for addr in addrlist: # for all possibilities of this memref
#                node = getCFGNode(addr)
#                codenode = node.to_codenode()
#                if codenode in loop.body_nodes: # if memref is inside loop
#                    apron.addCount(i, maxexecs[loop])
#                    # TODO Ojo: puede estar en una función que se llame varias veces dentro de un bucle, o en una función que se llame varias veces dentro de otra
#                    break


## MAIN
part_time = time.time()

# test command line arguments
#if len(sys.argv) != 3: # select domain
#    print("usage:", sys.argv[0], "<oct|pk|pkeq|ap_ppl> <binaryfile>")
#    sys.exit()
#domain = sys.argv[1]
#filename = sys.argv[2]
if len(sys.argv) != 2: # force domain
    print("usage:", sys.argv[0], "<binaryfile>")
    sys.exit()
domain = "pkeq"
filename = sys.argv[1]

# Set-up project from binary file
proj = angr.Project(filename, auto_load_libs=False) # carga ejecutable
main = proj.loader.main_object.get_symbol("main")
#proj = angr.Project("./simple.elf", auto_load_libs=False) # carga ejecutable
#proj = angr.Project("./if.elf", auto_load_libs=False) # carga ejecutable
#maxIterations={0x82bc: 100} # if.elf
#print(maxIterations[0x82bc])
#state = proj.factory.entry_state()
state = proj.factory.blank_state(addr=main.rebased_addr)
# store rdx to memory at 0x1000
#state.mem[0x1000].uint64_t = state.regs.rdx
# dereference rbp
#state.regs.rbp = state.mem[state.regs.rbp].uint64_t.resolved
anyStoreToUnknownAddr = False

flowfacts.get_flowfacts(filename)
print("Flow facts:", flowfacts.maxLoopIter)

# Build CFG and loopInfo
#useCFGEmulated = False
useCFGEmulated = True
if useCFGEmulated:
    #cfg = proj.analyses.CFGEmulated(normalize=True, context_sensitivity_level=3) # genera CFG
    cfg = proj.analyses.CFGEmulated(normalize=True, context_sensitivity_level=3, starts=[main.rebased_addr], initial_state=state) # genera CFG
    #cfg = proj.analyses.CFGEmulated(normalize=True, context_sensitivity_level=3, keep_state=True, enable_advanced_backward_slicing=True) # genera CFG
    #cfg.normalize() # genera correctamente bloques básicos, si no hay bloques incluidos en otros
    print("CFG (emulated) has %d nodes and %d edges" % (len(cfg.graph.nodes()), len(cfg.graph.edges())))
    # TODO: identify apron nodes by node._hash (int64_t) instead of node.addr
else:
    cfg = proj.analyses.CFG(normalize=True) # genera CFG (CFGFast)
    #cfg.normalize() # genera correctamente bloques básicos, si no hay bloques incluidos en otros
    print("CFG (fast) has %d nodes and %d edges" % (len(cfg.graph.nodes()), len(cfg.graph.edges())))

loopInfo = proj.analyses.LoopFinder() # Set-up loop information
# Locate main()
fmain = cfg.kb.functions.function(name="main") # main function
#if len(cfg.get_all_nodes(fmain.addr)) == 1: # main CFG node
#    mainNode = cfg.get_any_node(fmain.addr) # main CFG node
mainNode = cfg.model.get_any_node(fmain.addr) # main CFG node
mainBlock = mainNode.block # main basic block

# load apron interface
apron = CDLL("./libpolygaz.so")
#numcfgnodes = getNumNodes(fmain)
#nodeset = get_node_set(fmain)
nodeset = cfg.graph.nodes()
#print("nodeset:", nodeset)
numcfgnodes = len(nodeset)
#numcfgnodes = len(fmain.nodes)
init_apron(numcfgnodes, domain)

# Init system registers
registers = []
init_regs()

#### Stack limits
resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY, resource.RLIM_INFINITY)) # set system stack limit to unlimited
print("System stack limit:", resource.getrlimit(resource.RLIMIT_STACK))
# Print max stack depth (default: 1000)
#sys.setrecursionlimit(20000) # works with default ulimit stack size (8192 KiB)
sys.setrecursionlimit(1000000) # test: $ ulimit -s unlimited
print("Python recursion limit (stack depth):", sys.getrecursionlimit())

print("--- Init time:", round(time.time() - part_time, 2), "seconds ---")
part_time = time.time()

tested_loop = 0 # global required for induction variables
# test exploreLoop
#mibucle=33468
apron.newNodeState(set_node_id(mainNode), mainNode.addr)
#alreadyExplored = []
#modifiedNodes = []
visits = {} # dictionary of CFGNode:visits
#mem = {} # dictionary of constant values stored in memory
#anyStoreToUnknownAddr = False
#lastNode = cfg.get_any_node(fmain.ret_sites[0].addr)

if fmain.ret_sites:
    lastNode = cfg.model.get_any_node(fmain.ret_sites[0].addr)
    if lastNode.successors:
        endNode = lastNode.successors[0]
    else:
        endNode = None
else: # bad CFG
    print("CFG has not been correctly generated.")
    sys.exit(-1)

IDOMSTART = mainNode
IDOM = networkx.immediate_dominators(cfg.graph, IDOMSTART)
start = IDOMSTART
idom = IDOM
#path=exploreLoop(mainNode, lastNode, mainNode,[])
#print("Modified: ",len(modifiedNodes))

# not required for the first analysis
reginreg = {}
regintemp = {}

# previous numcfgnodes = len(cfg.graph.nodes) gives too much nodes
# we want just main() and called functions, so set to 0 an increment for each call
#numcfgnodes = 0
MemAccessAlreadySet = False
# TODO: exploreLoop2 analyzes until reaching endNode.
# A more robust implementation should analyze discarding the nodes in nodeset
#exploreLoop2(mainNode, endNode, mainNode, mainNode)
exploreLoop2(mainNode, nodeset, mainNode, mainNode)
MemAccessAlreadySet = True

#actualnodeset = [] # global to compare with loopnodeset
#if len(visits) != numcfgnodes:
if len(visits) < numcfgnodes:
    print("INFO: visited", len(visits), "of", numcfgnodes, "nodes")
else:
    print("ERROR: visited", len(visits), "of", numcfgnodes, "nodes")
    print("ERROR: Continuing, but CFG may be incorrect")
#compareNodes(visits, accounted)
actualnodeset = compareNodes(visits, nodeset)
#else:
#    actualnodeset = nodeset

print("")
print("--- Memory Reference analysis time:", round(time.time() - part_time, 2), "seconds ---")
print("")

apron.printAbstractMemRefs()


part_time = time.time()
find_induction_vars()

#print("")
#print("")
#print("Induction Variables")
#print("")
#apron.printIndVars()
#print("")
#print("end find induction vars")

loopindvars = indvars.getInfo()
print("")
print("--- Induction variable analysis time:", round(time.time() - part_time, 2), "seconds ---")
print("")
indvars.printInfo()
#print("")
#apron.printAbstractMemRefs()
#print("")

part_time = time.time()
genReuseTheoryVectors(idom, start, loopindvars)
print("")
print("--- Reuse theory vector generation time:", round(time.time() - part_time, 2), "seconds ---")
print("")
#apron.printAbstractMemRefs()
apron.printReuseTheoryMemRefs()

#print("")
#print("")
#print("induction test")
#apron.analyzeMemRefs()
#
##print("induction vars:")
##apron.printInductionVars()
##print("end ind vars")
#
#alreadyExplored = []
#induction()
#print("end induction")
#
##print("induction vars:")
##apron.printInductionVars()
##print("end ind vars")
#
#apron.printInductionMemRefs()
#
#
#apron.genReuseTheoryMemRefs3()
#
#apron.printAbstractMemRefs()
#
#apron.printReuseTheoryMemRefs()
#
#sys.exit()


part_time = time.time()
cacheLineSize = 64 # bytes (default intel/arm L1: 64 B)
calcGroupReuse(idom, start, cacheLineSize)
apron.calcFirstGR()
print("")
print("--- Group Reuse calculation time:", round(time.time() - part_time, 2), "seconds ---")
print("")
apron.printReuseTheoryMemRefs()

#alreadyExplored = []
##get_step_and_base(loopInfo.loops[3], 16) # simple.elf
#get_step_and_base(loopInfo.loops[6], 16) # bsort_O1.elf
#

setMaxExecs()
apron.classify(cacheLineSize) # Unknown (AM), Scalar GroupR (AH), Scalar NoR (Single Miss), Scalar SelfR (First Miss), Array GroupR (Always Hit), Array SelfR shortStride (calc), Array SelfR longStride (AM)
#calcDistances(idom, start)
# calcGRDistance()
#
