#!angr/bin/python3
# coding=utf-8

import sys
import angr # grafo
import re


## Gets any function containing addr (basicblock first addr),
#  reachable from function start
def get_function_from(addr, start):
    if addr in start.block_addrs:
        return start
    else:
        for call in list(start.get_call_sites()):
            calledf = start.get_call_target(call)
            if start != cfg.kb.functions[calledf]:
                function = get_function_from(addr, cfg.kb.functions[calledf])
                if function:
                    return function
            else:
                print("recursive call (not supported) detected:", start.name)
                sys.exit(-1)
        return None

## Prints flowfacts, with a format similat to otawa .ff
def print_ff(filename, start):
    prevfunction = None
    parent = {}
    with open(filename, "w") as f:
        #prevloop = None
        for loop in loopInfo.loops:
            function = get_function_from(loop.entry.addr, start)
            if function:
                #print("loop at", hex(loop.entry.addr), "in", function.name, "is reachable")
                # Separate functions with newline
                if function != prevfunction:
                    print(file=f)
                    print("// Function", function.name, file=f)
                    prevfunction = function
                # get calls info
                if loop.has_calls:
                    callstr = ", has calls"
                else:
                    callstr = ", no calls"
                # get subloops info
                if loop.subloops:
                    subloopstr = ", subloops:"
                    for subloop in loop.subloops:
                        subloopstr += " " + hex(subloop.entry.addr)
                        parent[subloop.entry.addr] = loop.entry.addr
                else:
                    subloopstr = ", no subloops"
                # set nesting spaces (assumes parents are listed above)
                nesting = ""
                thisloop = loop.entry.addr
                while thisloop in parent:
                    nesting += "    "
                    thisloop = parent[thisloop]
                print(nesting + "loop \"" + function.name + "\" +", hex(loop.entry.addr - function.addr), "?; //", hex(loop.entry.addr) + callstr + subloopstr, file=f)
            else:
                print("loop at", hex(loop.entry.addr), "not reachable from", start.name)


## Main
if len(sys.argv) == 1:
    print("usage:", sys.argv[0], "<binaryfile>")
    sys.exit()

# Set max stack depth (default: 1000)
sys.setrecursionlimit(20000)
#print("Max stack depth:", sys.getrecursionlimit())

name = re.match(".+\.elf$", sys.argv[1])
if name:
    #print("match")
    #print(name[0])
    ffname = sys.argv[1][:-3] + "ff"
else:
    #print("not match")
    ffname = sys.argv[1] + ".ff"

# Set-up project from binary file
proj = angr.Project(sys.argv[1], auto_load_libs=False) # carga ejecutable

# Build CFG and loopInfo
cfg = proj.analyses.CFG() # genera CFG (CFGFast)
#cfg = proj.analyses.CFGEmulated(keep_state=True, state_add_options=angr.sim_options.refs, context_sensitivity_level=2) # genera CFG 
cfg.normalize() # genera correctamente bloques básicos, si no hay bloques 

loopInfo = proj.analyses.LoopFinder() # Set-up loop information

fmain = cfg.kb.functions.function(name="main") # main function
print_ff(ffname, fmain)
